//
//  PulseView.swift
//  PeekPop
//
//  Created by Emran on 2/10/16.
//  Copyright © 2016 Frederik Jacques. All rights reserved.
//

import UIKit
import Material
import SwiftAddressBook

class PulseView: UIViewController {
    var window: UIWindow?

    @IBOutlet weak var imageview2: MaterialPulseView!
    @IBOutlet weak var imagview: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        let data = NSData(contentsOfURL: NSURL(string: "http://www.motorcycledaily.com/wp-content/uploads/2015/09/093015bottom.jpg")!)
        let data = NSData(contentsOfURL: NSURL(string: "http://amommyssweetblogdesign.com/wp-content/uploads/2015/10/Hot-Air-Balloons-uhd-wallpapers.jpg")!)
        NSLog("%u", (data?.length)!)
        NSLog("File size is : %.2f MB", Float(data!.length / 1024) / 1024)
        let somethin = data?.gzippedDataWithCompressionLevel(1)
        NSLog("%u", somethin!.length)
        NSLog("File size is : %.2f MB", Float(somethin!.length / 1024) / 1024)
        
        imagview.image = UIImage(data: data!)
        
        let backdata = somethin?.gunzippedData()
        imageview2.image = UIImage(data: backdata!)
        NSLog("%u", backdata!.length)

                // Do any additional setup after loading the view.
        
        // Toggle SideNavigationViewController.
        let img: UIImage? = UIImage(named: "harbour")
        let fabButton: FabButton = FabButton()
        fabButton.setImage(img, forState: .Normal)
        fabButton.setImage(img, forState: .Highlighted)
        fabButton.addTarget(self, action: "handleFabButton", forControlEvents: .TouchUpInside)
        
        view.addSubview(fabButton)
        fabButton.translatesAutoresizingMaskIntoConstraints = false
        MaterialLayout.alignFromBottomRight(view, child: fabButton, bottom: 16, right: 16)
        MaterialLayout.size(view, child: fabButton, width: 64, height: 64)
    }
    
    func addImageCardViewAction() {
        let imageCardView: ImageCardView = ImageCardView()
        
        // Image.
        let size: CGSize = CGSizeMake(UIScreen.mainScreen().bounds.width - CGFloat(40), 150)
        imageCardView.image = UIImage.imageWithColor(MaterialColor.cyan.darken1, size: size)
        
        // Title label.
        let titleLabel: UILabel = UILabel()
        titleLabel.text = "Welcome Back!"
        titleLabel.textColor = MaterialColor.white
        titleLabel.font = RobotoFont.mediumWithSize(24)
        imageCardView.titleLabel = titleLabel
        imageCardView.titleLabelInset.top = 100
        
        // Detail label.
        let detailLabel: UILabel = UILabel()
        detailLabel.text = "It’s been a while, have you read any new books lately?"
        detailLabel.numberOfLines = 0
        imageCardView.detailLabel = detailLabel
        
        // Yes button.
        let btn1: FlatButton = FlatButton()
        btn1.pulseColor = MaterialColor.cyan.lighten1
        btn1.pulseScale = false
        btn1.setTitle("Yes, Please", forState: .Normal)
        btn1.setTitleColor(MaterialColor.cyan.darken1, forState: .Normal)
        btn1.addTarget(self, action: "btnTapped:", forControlEvents: .TouchUpInside)
        // No button.
        let btn2: FlatButton = FlatButton()
        btn2.pulseColor = MaterialColor.cyan.lighten1
        btn2.pulseScale = false
        btn2.setTitle("I don't want", forState: .Normal)
        btn2.setTitleColor(MaterialColor.cyan.darken1, forState: .Normal)
        btn2.addTarget(self, action: "btnTapped:", forControlEvents: .TouchUpInside)

        // Add buttons to left side.
//        imageCardView.leftButtons = [btn1, btn2]
        imageCardView.rightButtons = [btn1, btn2]
        
        // To support orientation changes, use MaterialLayout.
        view.addSubview(imageCardView)
        imageCardView.translatesAutoresizingMaskIntoConstraints = false
        MaterialLayout.alignFromTop(view, child: imageCardView, top: 100)
        MaterialLayout.alignToParentHorizontally(view, child: imageCardView, left: 20, right: 20)
    }

    func btnTapped(sender: AnyObject) {
        print("Button tapped")
    }
    // FabButton handler.
    func handleFabButton() {
        sideNavigationViewController?.toggleLeftView()
    }

    @IBAction func loadContacts() {
        SwiftAddressBook.requestAccessWithCompletion({ (success, error) -> Void in
            if success {
                
                var contactsArray = [NSDictionary]()
                
                if let people = swiftAddressBook?.allPeople {
                    for person in people {
                        var tempNumbersArray = [String]()

                        let count = person.phoneNumbers?.count
                        for var i = 0; i < count; ++i {
                            let number = person.phoneNumbers![i].value
                            tempNumbersArray.append(number)
                        }
                        
                        if let name = person.compositeName {
                            let tempNameDict = NSDictionary(dictionary: ["name": name, "phone": tempNumbersArray])
                            contactsArray.append(tempNameDict)
                        }
                        let somet = person.modificationDate
                        print(somet)
                        let ss = person.creationDate
                        print(ss)
                        let id = person.recordID
                        
                        print("record id : \(id)")
                    }
                }
                
                let paramsJSON = JSON(contactsArray)
                print(paramsJSON)
                
                if let convertedddata = self.jsonToNSData(paramsJSON.rawValue) {
//                    print(convertedddata)
                    NSLog("%u", convertedddata.length)
                    NSLog("File size is : %.2f MB", Float(convertedddata.length / 1024) / 1024)
                    let somethinAgain = convertedddata.gzippedDataWithCompressionLevel(1)
                    NSLog("%u", (somethinAgain?.length)!)
                    NSLog("File size with compression level 100 is : %.2f MB", Float(somethinAgain!.length / 1024) / 1024)
                    NSLog("length: %i", somethinAgain!.length)

                    
                    let decompress = somethinAgain?.gunzippedData()
                    
                    let backjson = self.nsdataToJSON(decompress!)
//                    print(backjson)
                }
                
            }
            else {
                
                self.askAgain()
                //no success. Optionally evaluate error
            }
        })
    }
    // Convert from NSData to json object
    func nsdataToJSON(data: NSData) -> AnyObject? {
        do {
            return try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }

    // Convert from JSON to nsdata
    func jsonToNSData(json: AnyObject) -> NSData?{
        do {
            return try NSJSONSerialization.dataWithJSONObject(json, options: NSJSONWritingOptions.PrettyPrinted)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil;
    }

    func askAgain() {
        let alertController = UIAlertController(title: "Need Permission", message: "I cannot proceed without permission", preferredStyle: .Alert)
        
        let grantAction = UIAlertAction(title: "Change Permission", style: .Default, handler: { action in
            let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                UIApplication.sharedApplication().openURL(url)
            }
        })
        alertController.addAction(grantAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func gotomaterial(sender: AnyObject) {
        let textField: TextField = TextField(frame: CGRectMake(57, 100, 300, 24))
        textField.placeholder = "First Name"
        textField.font = RobotoFont.regularWithSize(20)
        textField.textColor = MaterialColor.black
        
        textField.titleLabel = UILabel()
        textField.titleLabel!.font = RobotoFont.mediumWithSize(12)
        textField.titleLabelColor = MaterialColor.grey.lighten1
        textField.titleLabelActiveColor = MaterialColor.blue.accent3
        textField.clearButtonMode = .WhileEditing
        
        // Add textField to UIViewController.
        view.addSubview(textField)
    }

}
